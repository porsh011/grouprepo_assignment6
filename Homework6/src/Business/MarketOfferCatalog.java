/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.List;

/**
 *
 * @author Pornima
 */
class MarketOfferCatalog {
    private List<MarketOffer> marketOffer;

    public List<MarketOffer> getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(List<MarketOffer> marketOffer) {
        this.marketOffer = marketOffer;
    }
    
}
