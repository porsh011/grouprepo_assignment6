/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pornima
 * 
 * 
 *
 */


public class Business {
    private SupplierDirectory supplierdir;
    private MarketOfferCatalog marketoffercatalog;
    private MarketList marketlist;

    public SupplierDirectory getSupplierdir() {
        return supplierdir;
    }

    public void setSupplierdir(SupplierDirectory supplierdir) {
        this.supplierdir = supplierdir;
    }

    public MarketOfferCatalog getMarketoffercatalog() {
        return marketoffercatalog;
    }

    public void setMarketoffercatalog(MarketOfferCatalog marketoffercatalog) {
        this.marketoffercatalog = marketoffercatalog;
    }

    public MarketList getMarketlist() {
        return marketlist;
    }

    public void setMarketlist(MarketList marketlist) {
        this.marketlist = marketlist;
    }
    
    
    
}
