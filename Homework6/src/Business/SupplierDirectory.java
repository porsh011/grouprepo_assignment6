/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.List;
//import java.util.function.Supplier;

/**
 *
 * @author Pornima
 */
class SupplierDirectory {
    private List<Supplier> supplierlist;

    public List<Supplier> getSupplierlist() {
        return supplierlist;
    }

    public void setSupplierlist(List<Supplier> supplierlist) {
        this.supplierlist = supplierlist;
    }
    
}
