/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.List;

/**
 *
 * @author Pornima
 */
public class UserAccountDirectory {
    private List<UserAccount> userAccount;

    public List<UserAccount> getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(List<UserAccount> userAccount) {
        this.userAccount = userAccount;
    }
    
}
