/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pornima
 */
public class OrderItem {
    int OrderQuantity;
    int PaidPrice;
    Product product;
    int OrderItemTotal;

    public int getOrderItemTotal() {
        return OrderItemTotal;
    }

    public void setOrderItemTotal(int OrderItemTotal) {
        this.OrderItemTotal = OrderItemTotal;
    }
    
    

    public int getOrderQuantity() {
        return OrderQuantity;
    }

    public void setOrderQuantity(int OrderQuantity) {
        this.OrderQuantity = OrderQuantity;
    }

    public int getPaidPrice() {
        return PaidPrice;
    }

    public void setPaidPrice(int PaidPrice) {
        this.PaidPrice = PaidPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
}
