/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pornima
 */
class MarketOffer {
    private Product product;
    private Customer customer;
    private int pricerange;
    private int floorprice;
    private int marketprice;
    private int targetprice;
    private int calrevenue;
    private int dynamicrange;

    public int getCalrevenue() {
        return calrevenue;
    }

    public void setCalrevenue(int calrevenue) {
        this.calrevenue = calrevenue;
    }

    public int getDynamicrange() {
        return dynamicrange;
    }

    public void setDynamicrange(int dynamicrange) {
        this.dynamicrange = dynamicrange;
    }
    
    

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getPricerange() {
        return pricerange;
    }

    public void setPricerange(int pricerange) {
        this.pricerange = pricerange;
    }

    public int getFloorprice() {
        return floorprice;
    }

    public void setFloorprice(int floorprice) {
        this.floorprice = floorprice;
    }

    public int getMarketprice() {
        return marketprice;
    }

    public void setMarketprice(int marketprice) {
        this.marketprice = marketprice;
    }

    public int getTargetprice() {
        return targetprice;
    }

    public void setTargetprice(int targetprice) {
        this.targetprice = targetprice;
    }
    
    
}
