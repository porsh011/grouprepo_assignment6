/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Pornima
 */
class Supplier {
    private String name;
    private String location;
    private ProductCatalog prodcatalog;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ProductCatalog getProdcatalog() {
        return prodcatalog;
    }

    public void setProdcatalog(ProductCatalog prodcatalog) {
        this.prodcatalog = prodcatalog;
    }
            
    
}
