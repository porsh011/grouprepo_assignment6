/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Business;
import Business.EmployeeDirectory;
import Business.MarketDirectory;
import Business.MarketOfferDirectory;
import Business.MasterOrderDirectory;
import Business.Person;
import Business.SupplierDirectory;
import Business.UserAccountDirectory;
/**
 *
 * @author RSE
 */
public class Business {
    
    private String name;
    private SupplierDirectory supplierDirectory;
    private MarketOfferDirectory marketOfferDirectory;
    private MarketDirectory marketDirectory;
    private EmployeeDirectory employeeDirectory;
    private MasterOrderDirectory masterOrderDirectory;
    private UserAccountDirectory userAccountDirectory;
    
    public Business(String n) {
        
        name = n;
        employeeDirectory = new EmployeeDirectory();
        marketDirectory = new MarketDirectory();
        marketOfferDirectory = new MarketOfferDirectory();
        masterOrderDirectory = new MasterOrderDirectory();
        supplierDirectory = new SupplierDirectory();
        userAccountDirectory = new UserAccountDirectory();
        
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MarketOfferDirectory getMarketOfferDirectory() {
        return marketOfferDirectory;
    }

    public void setMarketOfferDirectory(MarketOfferDirectory marketOfferDirectory) {
        this.marketOfferDirectory = marketOfferDirectory;
    }

    public MarketDirectory getMarketDirectory() {
        return marketDirectory;
    }

    public void setMarketDirectory(MarketDirectory marketDirectory) {
        this.marketDirectory = marketDirectory;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public MasterOrderDirectory getMasterOrderDirectory() {
        return masterOrderDirectory;
    }

    public void setMasterOrderDirectory(MasterOrderDirectory masterOrderDirectory) {
        this.masterOrderDirectory = masterOrderDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }
    
    
}
