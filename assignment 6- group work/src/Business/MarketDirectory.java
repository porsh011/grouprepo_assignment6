/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class MarketDirectory {
    
    private ArrayList<Market> marketDirectory;
    
    public MarketDirectory() {
        marketDirectory = new ArrayList<Market>();
    }

    public ArrayList<Market> getMarketDirectory() {
        return marketDirectory;
    }

    public void setMarketDirectory(ArrayList<Market> marketDirectory) {
        this.marketDirectory = marketDirectory;
    }
    
    public Market addMarket(Market m) {
        marketDirectory.add(m);
        return m;
    }
    
    public void removeMarket(Market m) {
        marketDirectory.remove(m);
    }
}
