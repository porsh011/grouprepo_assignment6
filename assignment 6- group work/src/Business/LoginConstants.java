/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 *  */
public class LoginConstants {
    
    public static final String NOT_FOUND = "not found";
    public static final String ACTIVE = "active";
    public static final String INACTIVE = "inactive";
    public static final String HR = "hr";
    public static final String SYSTEM_ADMIN = "admin";
}
