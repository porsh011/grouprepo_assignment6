/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class UserAccountDirectory {
    
    private ArrayList<UserAccount> userAccount;
    
    
    public UserAccountDirectory() {
        userAccount = new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(ArrayList<UserAccount> userAccount) {
        this.userAccount = userAccount;
    }
    
    public UserAccount addUserAccount(UserAccount ua) {
        userAccount.add(ua);
        return ua;
    }
    
    public void removeUserAccount(UserAccount ua) {
        userAccount.remove(ua);
    }
    
    public UserAccount searchUserAccount(String Keyword) {
        for(UserAccount u : userAccount) {
                if(Keyword.equals(u.getUsername())) {
                return u;
            }
        }
        return null;
    }
    
    public UserAccount isValidUser(String un, String ps) {
        for(UserAccount a : userAccount) {
            if(a.getUsername().equals(un) && a.getPassword().equals(ps)) {
                return a;
            }
        }
        
        return null;
    }
}
