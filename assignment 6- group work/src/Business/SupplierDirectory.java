/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class SupplierDirectory {
    
    private ArrayList<Supplier> supplierlist;
    
    public SupplierDirectory() {
        supplierlist = new ArrayList<Supplier> ();
    }

    public ArrayList<Supplier> getSupplierlist() {
        return supplierlist;
    }

    public void setSupplierlist(ArrayList<Supplier> supplierlist) {
        this.supplierlist = supplierlist;
    }
    
    public Supplier addSupplier(Supplier supplier) {
        
        supplierlist.add(supplier);
        return supplier;
    }
    
    public void removeSupplier(Supplier s) {
        supplierlist.remove(s);
    }
}
