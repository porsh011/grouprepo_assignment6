/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class ProductCatalog {
    
    private ArrayList<Product> product;
    
    public ProductCatalog() {
        product = new ArrayList<Product>();
    }

    public ArrayList<Product> getProduct() {
        return product;
    }

    public void setProduct(ArrayList<Product> product) {
        this.product = product;
    }
    
    public Product addProduct(Product p) {
        product.add(p);
        return p;
    }
    
    public void removeProduct(Product p) {
        product.remove(p);
    }
}
