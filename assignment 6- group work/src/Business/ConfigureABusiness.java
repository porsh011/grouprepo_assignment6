/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Person;
//import Business.HumanResources.PersonAccountDirectory.PersonAccountDirectory;
import Business.UserAccount;
//import Business.SystemAdmin.UserAccountDirectory;
import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Pornima
 */
public class ConfigureABusiness {
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;

    public ConfigureABusiness(){
        
        String loginFilePath = new File("CSV/Login.csv").getAbsolutePath();
        userAccountDirectory = new UserAccountDirectory();
        employeeDirectory = new EmployeeDirectory();
        
//        try{
//            userDir = new UserAccountDirectory();
//            ArrayList<UserAccount> usr= new ArrayList<>();
//            BufferedReader buffer= new BufferedReader(new FileReader(loginFilePath));
//            String line = "";       
//            while ((line = buffer.readLine()) != null) {          
//                String[] user=line.split(",");
//                if(user.length > 0) { 
//                    userDir.getUserAccountArrayList().add(new UserAccount(user[0], user[1], user[2],  Boolean.valueOf(user[3])));
//               
//                }
//            }
//        }
//        catch(IOException ex){
//            Logger.getLogger(ConfigureABusiness.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        
        try{
            BufferedReader buffer= new BufferedReader(new FileReader(loginFilePath));
            String line = "";       
            while ((line = buffer.readLine()) != null) {          
                String[] user=line.split(",");
                if(user.length > 0) { 
                    employeeDirectory.getPerson().add(new Person(user[0], user[1], new UserAccount(user[2], user[3], user[4], Boolean.valueOf(user[5]))));
                    userAccountDirectory.getUserAccount().add(new UserAccount(user[2], user[3], user[4], Boolean.valueOf(user[5])));
                }
            }
        }
        catch(IOException ex){
            Logger.getLogger(ConfigureABusiness.class.getName()).log(Level.SEVERE, null, ex);
        }
               
        //JAVA 8 coding style..
            
//            try{
//
//		userDir.setUserAccountArrayList(
//                        Files.lines(Paths.get(filePath)).map(line -> line.split(","))
//                                .map(x -> new UserAccount(x[0], x[1], x[2])).collect(Collectors.toList()));
//                
//
//            } catch (IOException ex) {
//                Logger.getLogger(Accounts.class.getName()).log(Level.SEVERE, null, ex);
//            }
               
        //JAVA 8 coding style..
            
//            try{
//
//		userDir.setUserAccountArrayList(
//                        Files.lines(Paths.get(filePath)).map(line -> line.split(","))
//                                .map(x -> new UserAccount(x[0], x[1], x[2])).collect(Collectors.toList()));
//                
//
//            } catch (IOException ex) {
//                Logger.getLogger(ConfigureABusiness.class.getName()).log(Level.SEVERE, null, ex);
//            }
               
        //JAVA 8 coding style..
            
//            try{
//
//		userDir.setUserAccountArrayList(
//                        Files.lines(Paths.get(filePath)).map(line -> line.split(","))
//                                .map(x -> new UserAccount(x[0], x[1], x[2])).collect(Collectors.toList()));
//                
//
//            } catch (IOException ex) {
//                Logger.getLogger(Accounts.class.getName()).log(Level.SEVERE, null, ex);
//            }
               
        //JAVA 8 coding style..
            
//            try{
//
//		userDir.setUserAccountArrayList(
//                        Files.lines(Paths.get(filePath)).map(line -> line.split(","))
//                                .map(x -> new UserAccount(x[0], x[1], x[2])).collect(Collectors.toList()));
//                
//
//            } catch (IOException ex) {
//                Logger.getLogger(ConfigureABusiness.class.getName()).log(Level.SEVERE, null, ex);
//            }
        

                System.out.println("userDir has :" + employeeDirectory.getPerson().size());

    }
    

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectoryDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }
    
    
    
}
