/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class Market {
    
    private String type;
    private ArrayList<Customer> customer;
    
    public Market() {
        customer = new ArrayList<Customer>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(ArrayList<Customer> customer) {
        this.customer = customer;
    }
    
    public Customer addCustomer(Customer c) {
        customer.add(c);
        return c;
    }
    
    public void removeCustomer(Customer c) {
        customer.remove(c);
    }
    
    public Customer searchCustomer(String keyword) {
        for(Customer c : customer) {
            if(c.getName().equals(keyword)) {
                return c;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return type;
    }
}
