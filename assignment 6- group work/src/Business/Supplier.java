/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author RSE
 */
public class Supplier {
    
    private String name;
    private String location;
    private ProductCatalog prodCatalog;
    
    public Supplier() {
        prodCatalog = new ProductCatalog();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ProductCatalog getProdCatalog() {
        return prodCatalog;
    }

    public void setProdCatalog(ProductCatalog prodCatalog) {
        this.prodCatalog = prodCatalog;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
