/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class EmployeeDirectory {
    
    private ArrayList<Person> person;
    
    public EmployeeDirectory() {
        person = new ArrayList<Person> ();
    }

    public ArrayList<Person> getPerson() {
        return person;
    }

    public void setPerson(ArrayList<Person> person) {
        this.person = person;
    }
    
    public Person addPerson(Person p) {
        person.add(p);
        return p;
    }
    
    public void removePerson(Person p) {
        person.remove(p);
    }
    
    public Person searchPersonRole(String Keyword) {
        for(Person p : person) {
                if(Keyword.equals(p.getName())) {
                return p;
            }
        }
        return null;
    }
}
