/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BusinessConfiguration;

import Business.Business;
import Business.Customer;
import Business.EmployeeDirectory;
import Business.Market;
import Business.MarketDirectory;
import Business.MarketOffer;
import Business.MarketOfferDirectory;
import Business.MasterOrderDirectory;
import Business.Order;
import Business.OrderItem;
import Business.Person;
import Business.Product;
import Business.Supplier;
import Business.SupplierDirectory;
import Business.UserAccount;
import Business.UserAccountDirectory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author RSE
 */

public class ConfigureABusiness {
    
    private Business business;
    private EmployeeDirectory employeeDirectory;
    private MarketDirectory marketDirectory;
    private MarketOfferDirectory marketOfferDirectory;
    private MasterOrderDirectory masterOrderDirectory;
    private SupplierDirectory supplierDirectory;
    private UserAccountDirectory userAccountDirectory;
    
    public static Business Initialize(String n) {
        Business business = new Business(n);
        
        UserAccountDirectory uad = business.getUserAccountDirectory();
        EmployeeDirectory ed = business.getEmployeeDirectory();
        SupplierDirectory sd = business.getSupplierDirectory();
        MarketOfferDirectory mod = business.getMarketOfferDirectory();
        MarketDirectory md = business.getMarketDirectory();
        MasterOrderDirectory mord = business.getMasterOrderDirectory();
        
        
        //initialize person info
        
        Person p1 = new Person();
        p1.setName("David");
        p1.setRole("System Admin");
        ed.addPerson(p1);
        
        Person p2 = new Person();
        p2.setName("Adan");
        p2.setRole("Human Resource");
        ed.addPerson(p2);
        
        Person p3 = new Person();
        p3.setName("Addison");
        p3.setRole("Market Offer Manager");
        ed.addPerson(p3);
        
        Person p4 = new Person();
        p4.setName("Adrian");
        p4.setRole("Sales");
        ed.addPerson(p4);
        
        Person p5 = new Person();
        p5.setName("Ainsley");
        p5.setRole("Supplier");
        ed.addPerson(p5);
        
        Person p6 = new Person();
        p6.setName("Addison");
        p6.setRole("Shareholder");
        ed.addPerson(p6);
        
        //initialize user account
        
        
        UserAccount ua1 = new UserAccount();
        ua1.setUsername("AdrianW");
        ua1.setAccountRole("System Admin");
        ua1.setPassword("aw10142017");
        ua1.setIsActive(true);
        ua1.setPerson(p1);
        uad.addUserAccount(ua1);
        
        UserAccount ua2 = new UserAccount();
        ua2.setUsername("AdrianWil");
        ua2.setAccountRole("System Admin");
        ua2.setPassword("aw10142013");
        ua2.setIsActive(false);
        ua2.setPerson(p1);
        uad.addUserAccount(ua2);
        
        UserAccount ua3 = new UserAccount();
        ua3.setUsername("davidJ3");
        ua3.setAccountRole("Human Resource");
        ua3.setPassword("dj09242016");
        ua3.setIsActive(true);
        ua3.setPerson(p2);
        uad.addUserAccount(ua3);
        
        UserAccount ua4 = new UserAccount();
        ua4.setUsername("davidJ2");
        ua4.setAccountRole("Human Resource");
        ua4.setPassword("davidj06142011");
        ua4.setIsActive(false);
        ua4.setPerson(p2);
        uad.addUserAccount(ua4);
        
        UserAccount ua5 = new UserAccount();
        ua5.setUsername("davidJ");
        ua5.setAccountRole("Market Offer Manager");
        ua5.setPassword("davidj03112008");
        ua5.setIsActive(true);
        ua5.setPerson(p3);
        uad.addUserAccount(ua5);
        
        UserAccount ua6 = new UserAccount();
        ua6.setUsername("adanb");
        ua6.setAccountRole("Sales");
        ua6.setPassword("adanb07082017");
        ua6.setIsActive(true);
        ua6.setPerson(p4);
        uad.addUserAccount(ua6);
        
        UserAccount ua7 = new UserAccount();
        ua7.setUsername("ainsW");
        ua7.setAccountRole("Supplier");
        ua7.setPassword("ainsW12232015");
        ua7.setIsActive(true);
        ua7.setPerson(p5);
        uad.addUserAccount(ua7);
        
        UserAccount ua8 = new UserAccount();
        ua8.setUsername("AddiB");
        ua8.setAccountRole("Shareholder");
        ua8.setPassword("AddiB12162013");
        ua8.setIsActive(true);
        ua8.setPerson(p6);
        uad.addUserAccount(ua8);
        
        //initialize supplier
        
        Supplier sp1 = new Supplier();
        sp1.setName("Canon");
        sp1.setLocation("Boston");
        sd.addSupplier(sp1);
        
        Supplier sp2 = new Supplier();
        sp2.setName("HP");
        sp2.setLocation("New York");
        sd.addSupplier(sp2);
        
        
        //initialize product detail
        
        Product pd1 = new Product();
        pd1.setName("Printer a");
        pd1.setDescription("colorful");
        pd1.setAvailability(30);
        sp1.getProdCatalog().addProduct(pd1);
        
        Product pd2 = new Product();
        pd2.setName("Printer b");
        pd2.setDescription("black & white");
        pd2.setAvailability(50);
        sp1.getProdCatalog().addProduct(pd2);
        
        Product pd3 = new Product();
        pd3.setName("Printer c");
        pd3.setDescription("colorful");
        pd3.setAvailability(15);
        sp1.getProdCatalog().addProduct(pd3);
        
        Product pd4 = new Product();
        pd4.setName("Printer ab");
        pd4.setDescription("Ink Jet");
        pd4.setAvailability(26);
        sp2.getProdCatalog().addProduct(pd4);
        
        Product pd5 = new Product();
        pd5.setName("Printer ed");
        pd5.setDescription("Laser");
        pd5.setAvailability(7);
        sp2.getProdCatalog().addProduct(pd5);
        
        //initialize customer
        
        Customer c1 = new Customer();
        c1.setName("Jush");
        c1.setAddress("No.123, Summer Street, Boston, MA");
        
        Customer c2 = new Customer();
        c2.setName("Pornima");
        c2.setAddress("New York");
        
        Customer c3 = new Customer();
        c3.setName("Kate");
        c3.setAddress("Chicago");
        
        Customer c4 = new Customer();
        c4.setName("Jerry");
        c4.setAddress("California");
        
        Customer c5 = new Customer();
        c5.setName("Lily");
        c5.setAddress("Huston");
        
        //initialize Market
        
        Market m1 = new Market();
        m1.setType("Commercial");
        m1.addCustomer(c1);
        md.addMarket(m1);
        
        Market m2 = new Market();
        m2.setType("Education");
        m2.addCustomer(c2);
        md.addMarket(m2);
        
        Market m3 = new Market();
        m3.setType("Beauty");
        m3.addCustomer(c3);
        md.addMarket(m3);
        
        Market m4 = new Market();
        m4.setType("Electronics");
        m4.addCustomer(c4);
        md.addMarket(m4);
        
        Market m5 = new Market();
        m5.setType("Home Decor");
        m5.addCustomer(c5);
        md.addMarket(m5);
        
        //initialize Market Offer
        
        MarketOffer mo1 = new MarketOffer();
        mo1.setFloorPrice(300);
        mo1.setCeilingPrice(500);
        mo1.setTargetPrice(350);
        mo1.setProduct(pd1);
        mod.addMarketOffer(mo1);
        
        MarketOffer mo2 = new MarketOffer();
        mo2.setFloorPrice(530);
        mo2.setCeilingPrice(600);
        mo2.setTargetPrice(400);
        mo2.setProduct(pd2);
        mod.addMarketOffer(mo2);
        
        MarketOffer mo3 = new MarketOffer();
        mo3.setFloorPrice(380);
        mo3.setCeilingPrice(700);
        mo3.setTargetPrice(650);
        mo3.setProduct(pd2);
        mod.addMarketOffer(mo3);
        
        MarketOffer mo4 = new MarketOffer();
        mo4.setFloorPrice(430);
        mo4.setCeilingPrice(660);
        mo4.setTargetPrice(500);
        mo4.setProduct(pd5);
        mod.addMarketOffer(mo4);
        
        
        
        //initialize Order
        
        Order od1 = new Order();
        od1.setStatus("Available");
        od1.setIssuedate("12/01/2017");
        od1.setCompletiondate("12/02/2017");
        od1.setShippingdate("12/04/2017");
        od1.setTotalAmount(1150);
        od1.setCustomer(c1);
        od1.setSalesPersonName(p4);

        mord.addOrder(od1);
       
        Order od2 = new Order();
        od2.setStatus("Available");
        od2.setIssuedate("12/01/2017");
        od2.setCompletiondate("12/02/2017");
        od2.setShippingdate("12/04/2017");
        od2.setTotalAmount(1530);

        mord.addOrder(od2);
       
        Order od3 = new Order();
        od3.setStatus("Available");
        od3.setIssuedate("12/01/2017");
        od3.setCompletiondate("12/02/2017");
        od3.setShippingdate("12/04/2017");
        od3.setTotalAmount(1830);

        mord.addOrder(od3);
       
        Order od4 = new Order();
        od4.setStatus("Available");
        od4.setIssuedate("12/01/2017");
        od4.setCompletiondate("12/02/2017");
        od4.setShippingdate("12/04/2017");
        od4.setTotalAmount(1590);

        mord.addOrder(od4);
        
        //initialize order Item
        
        OrderItem oi1 = new OrderItem();
        oi1.setOrderQuantity(6);
        oi1.setSalePrice(620);
        od1.addOrderItem(oi1);

        OrderItem oi2 = new OrderItem();
        oi2.setOrderQuantity(6);
        oi2.setSalePrice(700);
        od1.addOrderItem(oi2);

        OrderItem oi3 = new OrderItem();
        oi3.setOrderQuantity(6);
        oi3.setSalePrice(440);
        od2.addOrderItem(oi3);

        OrderItem oi4 = new OrderItem();
        oi4.setOrderQuantity(6);
        oi4.setSalePrice(560);
        od3.addOrderItem(oi4);
        
        OrderItem oi5 = new OrderItem();
        oi5.setOrderQuantity(2);
        oi5.setSalePrice(1560);
        od4.addOrderItem(oi5);
        
       
        
        return business;
          }
    /*public ConfigureABusiness(){
    UserAccountCSV();
    }
    
    public void UserAccountCSV()
    {
    try          
       {  
            UserAccountDirectory ua = business.getUserAccountDirectory();
            
            BufferedReader buffer= new BufferedReader(new FileReader("C:\\myProject\\Initialize_data\\UserAccount.csv"));
            String line = "";       
            while ((line = buffer.readLine()) != null) {          
            String[] userAccount=line.split(",");
            if(userAccount.length>0)
           {
               UserAccount usa = new UserAccount();
               usa.setUsername(userAccount[0]);
               usa.setPassword(userAccount[1]);
               usa.setAccountRole(userAccount[2]);
               if(userAccount[3] == "Active") {
                   usa.setIsActive(true);
               }
               else {
                   usa.setIsActive(false);
               }

               ua.addUserAccount(usa); 
            }
            }
            
       
        //System.out.println("Person "++ "size saf"+this.airplaneArray.size());
       
        
       
       }

     catch (FileNotFoundException e) {
         System.out.println("Exception ");
    } 
     catch (IOException e) {
         System.out.println("Exception ");
    }  
    
     //  catch (ArrayIndexOutOfBoundsException e) {
      //   System.out.println("Exception ");
      // }
    
    
       }*/
    
}
