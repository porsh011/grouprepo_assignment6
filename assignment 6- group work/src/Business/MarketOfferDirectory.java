/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class MarketOfferDirectory {
    
    private ArrayList<MarketOffer> marketOfferDirectory;
    
    public MarketOfferDirectory() {
        marketOfferDirectory = new ArrayList<MarketOffer>();
    }

    public ArrayList<MarketOffer> getMarketOfferDirectory() {
        return marketOfferDirectory;
    }

    public void setMarketOfferDirectory(ArrayList<MarketOffer> marketOfferDirectory) {
        this.marketOfferDirectory = marketOfferDirectory;
    }
    
    public MarketOffer addMarketOffer(MarketOffer m) {
        
        marketOfferDirectory.add(m);
        return m;
    }
    
    public void removeMarketOffer(MarketOffer m) {
        marketOfferDirectory.remove(m);
    }
}
