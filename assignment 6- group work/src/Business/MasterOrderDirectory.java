/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RSE
 */
public class MasterOrderDirectory {
    
    private ArrayList<Order> masterOrderDirectory;
    
    public MasterOrderDirectory() {
        masterOrderDirectory = new ArrayList<Order>();
    }

    public ArrayList<Order> getMasterOrderDirectory() {
        return masterOrderDirectory;
    }

    public void setMasterOrderDirectory(ArrayList<Order> masterOrderDirectory) {
        this.masterOrderDirectory = masterOrderDirectory;
    }
    
    public Order addOrder(Order o) {
        masterOrderDirectory.add(o);
        return o;
    }
    
    public void removeOrder(Order o) {
        masterOrderDirectory.remove(o);
    }
}
