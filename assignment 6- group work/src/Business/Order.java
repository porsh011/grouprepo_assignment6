/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author RSE
 */
public class Order {
    
    private ArrayList<OrderItem> orderItemList;
    private int orderId;
    private String status;
    private String issuedate;
    private String completiondate;
    private String shippingdate;
    private int totalAmount;
    private Person salesPersonName;
    private Customer customer;
    private Market market;
    private int OrderTotal;
    private static int count = 0;
    
    public Order() {
        count++;
        count = orderId;
        orderItemList = new ArrayList<OrderItem>();
        salesPersonName = new Person();
        customer = new Customer();
        market = new Market();
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIssuedate() {
        return issuedate;
    }

    public void setIssuedate(String issuedate) {
        this.issuedate = issuedate;
    }

    public String getCompletiondate() {
        return completiondate;
    }

    public void setCompletiondate(String completiondate) {
        this.completiondate = completiondate;
    }

    public String getShippingdate() {
        return shippingdate;
    }

    public void setShippingdate(String shippingdate) {
        this.shippingdate = shippingdate;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Person getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(Person salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getOrderTotal() {
        return OrderTotal;
    }

    public void setOrderTotal(int OrderTotal) {
        this.OrderTotal = OrderTotal;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }
    
    public OrderItem addOrderItem(OrderItem o) {
        orderItemList.add(o);
        return o;
    }
    
    public OrderItem addOrderItem(Product p, int q, int price)
    {
        OrderItem orderItem = new OrderItem();
        orderItem.setProduct(p);
        orderItem.setOrderQuantity(q);
        orderItem.setSalePrice(price);
        orderItemList.add(orderItem);
        return orderItem;
    }
    
    public void removeOrderItem(OrderItem p) {
        orderItemList.remove(p);
    }
    
    @Override
    public String toString() {
        return String.valueOf(orderId);
    }
}
