/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SalesRole;

import Business.Customer;
import Business.EmployeeDirectory;
import Business.Market;
import Business.MarketDirectory;
import Business.MarketOfferDirectory;
import Business.MasterOrderDirectory;
import Business.SupplierDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author RSE
 */
public class ManageCustomerJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageCustomerJPanel
     */
    private JPanel userProcessContainer;
    private EmployeeDirectory employeeDirectory;
    private MarketDirectory marketDirectory;
    private MarketOfferDirectory marketOfferDirectory;
    private MasterOrderDirectory masterOrderDirectory;
    private SupplierDirectory supplierDirectory;
    
    public ManageCustomerJPanel(JPanel userProcessContainer, EmployeeDirectory employeeDirectory, MarketDirectory marketDirectory, MarketOfferDirectory marketOfferDirectory, MasterOrderDirectory masterOrderDirectory, SupplierDirectory supplierDirectory) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.employeeDirectory = employeeDirectory;
        this.marketDirectory = marketDirectory;
        this.marketOfferDirectory = marketOfferDirectory;
        this.masterOrderDirectory = masterOrderDirectory;
        this.supplierDirectory = supplierDirectory;
        
        ComboBoxMarketType.removeAllItems();
        
        for(Market m : marketDirectory.getMarketDirectory()) {
            ComboBoxMarketType.addItem(m);
        }
        
        
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtCustomerName = new javax.swing.JTextField();
        btnServeCustomer = new javax.swing.JButton();
        btnReviewSalesOrderHistory = new javax.swing.JButton();
        btnReviewSalesCommission = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        ComboBoxMarketType = new javax.swing.JComboBox();

        jLabel1.setFont(new java.awt.Font("Palatino Linotype", 1, 24)); // NOI18N
        jLabel1.setText("Manage Customer -Sales Role");

        jLabel3.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        jLabel3.setText("Customer Name:");

        btnServeCustomer.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btnServeCustomer.setText("Serve Customer >");
        btnServeCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServeCustomerActionPerformed(evt);
            }
        });

        btnReviewSalesOrderHistory.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btnReviewSalesOrderHistory.setText("Review Sales Order History >");
        btnReviewSalesOrderHistory.setEnabled(false);
        btnReviewSalesOrderHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReviewSalesOrderHistoryActionPerformed(evt);
            }
        });

        btnReviewSalesCommission.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btnReviewSalesCommission.setText("Review Sales Commission >");
        btnReviewSalesCommission.setEnabled(false);
        btnReviewSalesCommission.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReviewSalesCommissionActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        jLabel4.setText("Market Type:");

        ComboBoxMarketType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(147, Short.MAX_VALUE)
                .addComponent(btnServeCustomer)
                .addGap(140, 140, 140))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(110, 110, 110)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnReviewSalesOrderHistory)
                            .addComponent(btnReviewSalesCommission)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCustomerName, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                            .addComponent(ComboBoxMarketType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(ComboBoxMarketType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtCustomerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnServeCustomer)
                .addGap(18, 18, 18)
                .addComponent(btnReviewSalesOrderHistory)
                .addGap(18, 18, 18)
                .addComponent(btnReviewSalesCommission)
                .addContainerGap(41, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnServeCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServeCustomerActionPerformed
        // TODO add your handling code here:
        Market assignedMarket = (Market) ComboBoxMarketType.getSelectedItem();
        Customer assignedCustomer = assignedMarket.searchCustomer(txtCustomerName.getText());
        
        ServeCustomerJPanel scjp = new ServeCustomerJPanel(userProcessContainer,employeeDirectory, marketDirectory, marketOfferDirectory, masterOrderDirectory, supplierDirectory, assignedMarket, assignedCustomer);
        userProcessContainer.add("ServeCustomerJPanel", scjp);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer); 
    }//GEN-LAST:event_btnServeCustomerActionPerformed

    private void btnReviewSalesCommissionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReviewSalesCommissionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnReviewSalesCommissionActionPerformed

    private void btnReviewSalesOrderHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReviewSalesOrderHistoryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnReviewSalesOrderHistoryActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox ComboBoxMarketType;
    private javax.swing.JButton btnReviewSalesCommission;
    private javax.swing.JButton btnReviewSalesOrderHistory;
    private javax.swing.JButton btnServeCustomer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtCustomerName;
    // End of variables declaration//GEN-END:variables
}
