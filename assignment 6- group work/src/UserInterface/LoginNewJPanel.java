/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

//import UserInterface.SystemAdmin.SystemAdminWorkAreaJPanel;
//import UserInterface.HumanResource.HumanResourcesWorkAreaJPanel;
import Business.LoginConstants;
import Business.EmployeeDirectory;
//import Business.HumanResources.PersonAccountDirectory.PersonAccountDirectory;
import Business.UserAccount;
import Business.UserAccountDirectory;
import UserInterface.AdminstrativeRole.AdminWorkAreaJPanel;
import UserInterface.HumanResourceRole.HRWorkAreaJPanel;

//import Business.SystemAdmin.UserAccountDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author adityatimmaraju
 */
public class LoginNewJPanel extends javax.swing.JPanel {

    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountdirectory;
    private JPanel userProcessContainer;
    /**
     * Creates new form LoginJPanel
     */
//    public LoginNewJPanel(JPanel userProcessContainer, EmployeeDirectory employeeDirectory, UserAccountDirectory userAccountDirectory) {
//        initComponents();
//        this.employeeDirectory = employeeDirectory;
//        this.userAccountdirectory = userAccountDirectory;
//        this.userProcessContainer=userProcessContainer;
//    }

    LoginNewJPanel(JPanel userProcessContainer, UserAccountDirectory userAccountDirectory, EmployeeDirectory employeeDirectory) {
       initComponents();
        this.employeeDirectory = employeeDirectory;
        this.userAccountdirectory = userAccountDirectory;
        this.userProcessContainer=userProcessContainer;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        userIdTxt = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        passwordTxt = new javax.swing.JPasswordField();
        reset = new javax.swing.JButton();
        login = new javax.swing.JButton();

        jLabel1.setText("UserId");

        jLabel2.setText("Password");

        reset.setText("Reset");
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });

        login.setText("Login");
        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 687, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 180, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(reset)
                            .addGap(117, 117, 117)
                            .addComponent(login))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(19, 19, 19)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(51, 51, 51)
                                    .addComponent(userIdTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(68, 68, 68)
                                    .addComponent(passwordTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGap(0, 181, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 405, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 129, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(5, 5, 5)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(userIdTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(5, 5, 5)
                            .addComponent(jLabel2))
                        .addComponent(passwordTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(48, 48, 48)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(reset)
                        .addComponent(login))
                    .addGap(0, 130, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
        // TODO add your handling code here:

        
        this.userIdTxt.setText(null);
        this.passwordTxt.setText(null);
    }//GEN-LAST:event_resetActionPerformed

    private void loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginActionPerformed
        // TODO add your handling code here:
        this.userAccountdirectory.getUserAccount().forEach(x -> System.out.println("UserName: " + x.getUsername() + " pwd: " + x.getPassword() + " active:" + x.isIsActive() + " role: " + x.getAccountRole()));
        String userId = this.userIdTxt.getText();
        String password = this.passwordTxt.getText();
        CardLayout layout = null;
        switch(this.userType(this.userAccountdirectory, userId, password)){
            case LoginConstants.SYSTEM_ADMIN:
            System.out.println("This is system Admin");

            AdminWorkAreaJPanel adminJPanel = new AdminWorkAreaJPanel(userProcessContainer, this.getUserAccountDetails(this.userAccountdirectory, userId, password),userAccountdirectory, employeeDirectory);
            userProcessContainer.add("AdminWorkAreaJPanel", adminJPanel);
            layout = (CardLayout) userProcessContainer.getLayout();
            //layout = (CardLayout) getContentPane().getLayout();,
            layout.next(userProcessContainer);
            break;
            case LoginConstants.HR:
            System.out.println("This is HR ");
            HRWorkAreaJPanel hrJPanel = new HRWorkAreaJPanel(userProcessContainer, this.getUserAccountDetails(this.userAccountdirectory, userId, password),userAccountdirectory, employeeDirectory);
            userProcessContainer.add("HRWorkAreaJPanel", hrJPanel);
            layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
            break;
            case LoginConstants.INACTIVE:
            System.out.println("Account Inactive");
            JOptionPane.showMessageDialog(null,"Account Inactive. Contact System Admin", "Login failed", JOptionPane.INFORMATION_MESSAGE);
            break;
            default:
            System.out.println("Oppss! account not found");
            JOptionPane.showMessageDialog(null,"Account Not Found", "Login failed", JOptionPane.INFORMATION_MESSAGE);
            break;
        }
    }//GEN-LAST:event_loginActionPerformed

     private String userType(UserAccountDirectory userAccountDirectory, String userId, String password){
        int index = userAccountDirectory.getUserAccount().indexOf(new UserAccount(userId, password));
        
        System.out.println("Index: " + index);
        if(index != -1){
            UserAccount userAccount =  userAccountDirectory.getUserAccount().get(index); 
            if(userAccount.isIsActive()){
                 return userAccount.getAccountRole();
            } else{
                return LoginConstants.INACTIVE;
            }
        }
        return LoginConstants.NOT_FOUND;
        
    }
    
    private UserAccount getUserAccountDetails(UserAccountDirectory userAccountDirectory, String userId, String password){
        int index = userAccountDirectory.getUserAccount().indexOf(new UserAccount(userId, password));
        return userAccountDirectory.getUserAccount().get(index); 
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton login;
    private javax.swing.JPasswordField passwordTxt;
    private javax.swing.JButton reset;
    private javax.swing.JTextField userIdTxt;
    // End of variables declaration//GEN-END:variables
}
