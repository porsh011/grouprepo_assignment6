/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AdminstrativeRole;

import Business.EmployeeDirectory;
import Business.MarketDirectory;
import Business.MarketOfferDirectory;
import Business.SupplierDirectory;
import Business.UserAccountDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author RSE
 */
public class AdminWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form AdminWorkAreaJPanel
     */
    private JPanel userProcessContainer;
    private SupplierDirectory supplierDirectory;
    private UserAccountDirectory userAccountDirectory;
    private EmployeeDirectory employeeDirectory;
    private MarketDirectory marketDirectory;
    private MarketOfferDirectory marketOfferDirectory;
    
    public AdminWorkAreaJPanel(JPanel userProcessContainer, SupplierDirectory supplierDirectory, UserAccountDirectory userAccountDirectory, EmployeeDirectory employeeDirectory, MarketDirectory marketDirectory, MarketOfferDirectory marketOfferDirectory) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.employeeDirectory = employeeDirectory;
        this.supplierDirectory = supplierDirectory;
        this.userAccountDirectory = userAccountDirectory;
        this.marketDirectory = marketDirectory;
        this.marketOfferDirectory = marketOfferDirectory;
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnMngSupplier = new javax.swing.JButton();
        btnMngUserAccount = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Palatino Linotype", 1, 24)); // NOI18N
        jLabel1.setText("Work Area -Aminstrative Role");

        btnMngSupplier.setText("Manage Supplier >");
        btnMngSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMngSupplierActionPerformed(evt);
            }
        });

        btnMngUserAccount.setText("Manage User Account >");
        btnMngUserAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMngUserAccountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(77, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(62, 62, 62))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnMngUserAccount)
                            .addComponent(btnMngSupplier))
                        .addGap(173, 173, 173))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(40, 40, 40)
                .addComponent(btnMngSupplier)
                .addGap(18, 18, 18)
                .addComponent(btnMngUserAccount)
                .addContainerGap(143, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnMngSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMngSupplierActionPerformed
        // TODO add your handling code here:
        ManageSuppliers msjp = new ManageSuppliers(userProcessContainer, supplierDirectory, marketDirectory, marketOfferDirectory);
        userProcessContainer.add("ManageSuppliers", msjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnMngSupplierActionPerformed

    private void btnMngUserAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMngUserAccountActionPerformed
        // TODO add your handling code here:
        ManageUserAccountJPanel muajp = new ManageUserAccountJPanel(userProcessContainer, userAccountDirectory, employeeDirectory);
        userProcessContainer.add("ManageUserAccountJPanel", muajp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnMngUserAccountActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnMngSupplier;
    private javax.swing.JButton btnMngUserAccount;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
