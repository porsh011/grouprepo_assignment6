/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AdminstrativeRole;

import Business.UserAccount;
import Business.UserAccountDirectory;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author RSE
 */
public class SearchUserAccountJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SearchUserAccountJPanel
     */
    private JPanel userProcessContainer;
    private UserAccountDirectory userAccountDirectory;
    
    public SearchUserAccountJPanel(JPanel userProcessContainer, UserAccountDirectory userAccountDirectory) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccountDirectory = userAccountDirectory;
    }
    
    public void populatedTable() {
        DefaultTableModel model = (DefaultTableModel) tblSearchResults.getModel();
        
        int rowcount = tblSearchResults.getRowCount();
        for(int i = rowcount - 1; i >= 0; i--) { 
            model.removeRow(i);
        }
        
        UserAccountDirectory resultsUad = new UserAccountDirectory();
        
        for(UserAccount us : userAccountDirectory.getUserAccount()) {
            if(!txtPerson.getText().equals("") && us.getPerson().getName().equals(txtPerson.getText())) {
                
                resultsUad.addUserAccount(us);
            }
            else if(!txtUsername.getText().equals("") && us.getUsername().equals(txtUsername.getText())) {
                //userAccountDirectory.searchUserAccount(txtUsername.getText());
                resultsUad.addUserAccount(us);
            }
            else if(!txtUserId.getText().equals("") && String.valueOf(us.getUserid()).equals(txtUserId.getText())) {
                resultsUad.addUserAccount(us);
            }
            else if(!txtAccountRole.getText().equals("") && us.getAccountRole().equals(txtAccountRole.getText())) {
                resultsUad.addUserAccount(us);
            }
            else if(CheckBoxActiveAccounts.isSelected() && us.isIsActive()) {
                resultsUad.addUserAccount(us);
            }
            else if(CheckBoxDisabledAccounts.isSelected() && us.isIsActive() == false) {
                resultsUad.addUserAccount(us);
            }
            
        }
        if(resultsUad == null) {
            JOptionPane.showMessageDialog(null, "Cannot find out! Please try another search.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        
        for(UserAccount us : resultsUad.getUserAccount()) {
            Object row[] = new Object[6];
            
            row[0] = us;
            row[1] = us.getUsername();
            row[2] = us.getPerson().getName();
            row[3] = us.getAccountRole();
            row[4] = us.getPassword();
            row[5] = us.isIsActive();
            
            model.addRow(row);
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnSearch = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtPerson = new javax.swing.JTextField();
        txtUsername = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtUserId = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtAccountRole = new javax.swing.JTextField();
        CheckBoxActiveAccounts = new javax.swing.JCheckBox();
        CheckBoxDisabledAccounts = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSearchResults = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Palatino Linotype", 1, 24)); // NOI18N
        jLabel1.setText("Search User Account- Admin");

        btnSearch.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        jLabel3.setText("Username:");

        jLabel4.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        jLabel4.setText("Person:");

        jLabel6.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        jLabel6.setText("User ID:");

        txtUserId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUserIdActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        jLabel7.setText("Account Role:");

        CheckBoxActiveAccounts.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        CheckBoxActiveAccounts.setText("Active Accounts");

        CheckBoxDisabledAccounts.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        CheckBoxDisabledAccounts.setText("Disabled Accounts");

        tblSearchResults.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        tblSearchResults.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User ID", "Username", "Person", "Account Role", "Password", "Active Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblSearchResults.setGridColor(new java.awt.Color(51, 51, 51));
        jScrollPane1.setViewportView(tblSearchResults);

        btnBack.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btnBack.setText("<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 5, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(34, 34, 34))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtUsername)
                                        .addComponent(txtUserId, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(txtPerson, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtAccountRole, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addComponent(btnSearch))
                                    .addComponent(CheckBoxActiveAccounts)
                                    .addComponent(CheckBoxDisabledAccounts))
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addGap(93, 93, 93)
                        .addComponent(btnDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnUpdate)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPerson, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addComponent(CheckBoxActiveAccounts))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(btnSearch))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CheckBoxDisabledAccounts)
                            .addComponent(txtUserId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAccountRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate)
                    .addComponent(btnBack)
                    .addComponent(btnDelete))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        populatedTable();
    }//GEN-LAST:event_btnSearchActionPerformed

    private void txtUserIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUserIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUserIdActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents(); 
        Component component = componentArray[componentArray.length - 1];
        ManageUserAccountJPanel manageUserAccount = (ManageUserAccountJPanel) component; 
        manageUserAccount.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        int selectRow = tblSearchResults.getSelectedRow();
        
        if(selectRow < 0) {
            JOptionPane.showMessageDialog(null, "Please at least one row to update.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        
        UserAccount selectedUserAccount = (UserAccount)tblSearchResults.getValueAt(selectRow, 0);
        userAccountDirectory.removeUserAccount(selectedUserAccount);
        JOptionPane.showMessageDialog(null, "User Account has been deleted");
        populatedTable();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        int selectRow = tblSearchResults.getSelectedRow();
        
        if(selectRow < 0) {
            JOptionPane.showMessageDialog(null, "Please at least one row to update.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        
        UserAccount selectedUserAccount = (UserAccount)tblSearchResults.getValueAt(selectRow, 0);
        UpdateUserAccountStatusJPanel uuasjp = new UpdateUserAccountStatusJPanel(userProcessContainer, selectedUserAccount);
        userProcessContainer.add("UpdateUserAccountStatusJPanel", uuasjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnUpdateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CheckBoxActiveAccounts;
    private javax.swing.JCheckBox CheckBoxDisabledAccounts;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblSearchResults;
    private javax.swing.JTextField txtAccountRole;
    private javax.swing.JTextField txtPerson;
    private javax.swing.JTextField txtUserId;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
